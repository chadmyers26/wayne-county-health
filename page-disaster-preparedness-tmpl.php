<?php
/**
 * Template Name: Disaster Preparedness Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="disaster-preparedness" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('disaster-preparedness-hero-widget')){
						dynamic_sidebar('disaster-preparedness-hero-widget');
					}
				?>
			</div>
			<div class="information-wrapper">
				<div class="disaster-preparedness-info">
					<div class="heading">
						<?php if( get_field('disaster-preparedness-info-heading') ): ?>
							<h3><?php the_field('disaster-preparedness-info-heading'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="description">
						<?php if( get_field('disaster-preparedness-info-description') ): ?>
							<p><?php the_field('disaster-preparedness-info-description'); ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="disaster-preparedness-links">
					<div class="disaster-preparedness-links-wrapper">
						<hr><div class="title"><p>Links</p></div><hr>
					</div>
					<div class="disaster-preparedness-links-info">
						<?php if( get_field('disaster-preparedness-links') ): ?>
							<?php the_field('disaster-preparedness-links'); ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="disaster-preparedness-pdfs">
					<div class="disaster-preparedness-pdfs-wrapper">
						<hr><div class="title"><p>PDFs</p></div><hr>
					</div>
					<div class="disaster-preparedness-pdfs-info">
						<?php if( get_field('disaster-preparedness-pdfs') ): ?>
							<?php the_field('disaster-preparedness-pdfs'); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
