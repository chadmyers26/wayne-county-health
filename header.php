<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wayne_County_Health
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/wayne-county-health/js/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/wayne-county-health/js/slick/slick-theme.css"/>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wayne-county-health' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<div class="logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="/wp-content/themes/wayne-county-health/assets/logo/WCHD Logo.svg" alt="Wayne County Health Logo"></a>
			</div>
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$wayne_county_health_description = get_bloginfo( 'description', 'display' );
			if ( $wayne_county_health_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $wayne_county_health_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<div class="nav-search">
			<nav id="site-navigation" class="main-navigation show" role="navigation" aria-label="<?php _e( "primary-menu", "textdomain" ); ?>">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'container' => false,
						'menu_id'        => 'primary-menu',
						'walker'         => new Aria_Walker_Nav_Menu(),
						'items_wrap' => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>'
					) );
				?>
			</nav><!-- #site-navigation -->

			<div id="masthead-search-form" class="masthead-search">
				<button type="button" class="search-icon-button show">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
					<style type="text/css">
						.st0{clip-path:url(#SVGID_2_);}
						.st1{fill:none;}
					</style>
					<g>
						<defs>
							<path id="SVGID_1_" d="M11.1,20.5c0-6.1,4.8-11,10.7-11s10.7,4.9,10.7,11s-4.8,11-10.7,11S11.1,26.6,11.1,20.5z M41.9,41
								l-9.6-10.7c2.3-2.6,3.7-6,3.7-9.8c0-8-6.4-14.5-14.2-14.5S7.7,12.5,7.7,20.5c0,8,6.4,14.5,14.2,14.5c2.9,0,5.6-0.9,7.8-2.4
								l9.7,10.8c0.3,0.4,0.8,0.6,1.3,0.6c0.4,0,0.8-0.1,1.2-0.5C42.5,42.9,42.5,41.8,41.9,41z"/>
						</defs>
						<clipPath id="SVGID_2_">
							<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
						</clipPath>
						<rect x="-3.9" y="-4.6" class="st0" width="57.8" height="59.1"/>
					</g>
					<path class="st1" d="M12.2,21.5c0-6.1,4.4-11,9.8-11s9.8,4.9,9.8,11s-4.4,11-9.8,11S12.2,27.6,12.2,21.5z M40.3,42l-8.8-10.7
						c2.1-2.6,3.4-6,3.4-9.8c0-8-5.8-14.5-13-14.5S9,13.5,9,21.5c0,8,5.8,14.5,13,14.5c2.6,0,5.1-0.9,7.1-2.4l8.8,10.8
						c0.3,0.4,0.7,0.6,1.2,0.6c0.4,0,0.8-0.1,1.1-0.5C40.8,43.9,40.8,42.8,40.3,42z"/>
				</svg>
				</button>
				<?php get_search_form(); ?>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
