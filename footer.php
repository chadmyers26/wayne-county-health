<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wayne_County_Health
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="social-media">
				<div class="youtube">
					<a href="https://www.youtube.com/channel/UCBjaGpI8eaYEztYXhzTmKEw?feature=embeds_subscribe_title" target="_blank"><img src="/wp-content/themes/wayne-county-health/assets/icons/Social Media/YouTube.svg" alt="YouTube Logo"></a>
				</div>

				<div class="twitter">
					<a href="https://twitter.com/WayneCoHD" target="_blank"><img src="/wp-content/themes/wayne-county-health/assets/icons/Social Media/Twitter.svg" alt="Twitter Logo"></a>
				</div>

				<div class="facebook">
					<a href="https://www.facebook.com/WayneCountyHealth/" target="_blank"><img src="/wp-content/themes/wayne-county-health/assets/icons/Social Media/Facebook.svg" alt="Facebook Logo"></a>
				</div>
			</div>
			<div class="locations">
				<div class="left">
					<?php
						if(is_active_sidebar('footer-location-left')){
						dynamic_sidebar('footer-location-left');
						}
					?>
				</div>
				<div class="right">
					<?php
						if(is_active_sidebar('footer-location-right')){
						dynamic_sidebar('footer-location-right');
						}
					?>
				</div>
			</div>
			<div class="copyright">
				<p>&copy; <?php echo "<script>var year = new Date(); document.write(year.getFullYear());</script>" ?> Wayne County Health Department | <a href="https://irongatecreative.com" target="_blank">Powered by IronGate Creative</a></p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/wayne-county-health/js/slick/slick.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.news-items').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		accessibility: true,
		arrows: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					vertical: true,
					swipe: true,
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: false,
					dots: true,
					arrows: false
				}
			}
		]
	});
});
</script>
</body>
</html>
