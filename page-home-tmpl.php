<?php
/**
 * Template Name: Home Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('home-hero-widget')){
						dynamic_sidebar('home-hero-widget');
					}
				?>
				<div class="hero-text-overlay">
					<p class="top-text">Where community</p>
					<p class="bottom-text">Matters</p>
					<img class="arrow" src="wp-content/themes/wayne-county-health/assets/icons/General/Arrow.svg" alt="More Content Below Icon">
				</div>
			</div>
			<div class="home-information-nav">
				<?php
					if(is_active_sidebar('home-information-text')){
					dynamic_sidebar('home-information-text');
					}
				?>
				<?php
					if(is_active_sidebar('home-information-nav')){
					dynamic_sidebar('home-information-nav');
					}
				?>
			</div>
			<div class="home-news-posts">
				<div class="home-news-posts-title-container">
					<hr><div class="title"><p>News</p></div><hr>
				</div>
				<div class="news-items-wrapper">
					<?php $the_query = new WP_Query(array(
							'post_type'=>'post',
							'post_status'=>'publish',
							'posts_per_page' => 3,
						));
					?>
					<?php if ( $the_query->have_posts() ) : ?>
					<ul class="news-item">
						<?php
						while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
							<li>
								<div class="image">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="new-item-content-wrapper">
									<div class="categories <?php $categories = get_the_category(); echo esc_html( $categories[0]->name ); ?>">
										<?php the_category(', '); ?>
									</div>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php wp_reset_postdata(); ?>
					<?php else: ?>
					<?php endif; ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
