/**
 * File search-toggle.js.
 *
 * Handles toggling the search bar and the main navigation, when the search icon is clicked.
 */
( function($) {
	"use strict";

	var menuItemsContainer = document.getElementById("wprmenu_menu_ul");
	$(menuItemsContainer).append('<div id="search-inputs" class="search-inputs"><form class="search-form" role="search" method="get" id="search-form" action="http://waynecountyhealth.flywheelsites.com"><label class="search-field"><span class="screen-reader-text">What are you searching for?</span><input type="text" id="s" name="s" class="search-input form-control" placeholder="Search"/></label><button type="submit" class="search-submit"><img src="/wp-content/themes/wayne-county-health/assets/icons/General/Search-Icon.svg" alt="Search Submit"></button></form></div>');

	var container, button, search, mainMenu, searchSubmit;

	container = document.getElementById("masthead-search-form");
	if ( ! container ) {
		return;
	}

	button = container.getElementsByClassName("search-icon-button")[0];
	if ("undefined" === typeof button ) {
		return;
	}

	searchSubmit = container.getElementsByClassName("search-submit")[0];
	if ("undefined" === typeof button ) {
		return;
	}

	search = document.getElementById("search-inputs");
	if ( ! search ) {
		return;
	}

	mainMenu = document.getElementById("site-navigation");
	if ( ! mainMenu ) {
		return;
	}

	function hasClass(element, className) {
		return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
	}

	button.onclick = function(event, target) {
		if (hasClass(search, "hide")) {
			mainMenu.className = mainMenu.className.replace("main-navigation show", "main-navigation hide");
			container.className = container.className.replace("masthead-search", "masthead-search full");
			button.className = button.className.replace("search-icon-button show", "search-icon-button hide");
			search.className = "search-inputs show";
		} else {
			search.className = search.className.replace("search-inputs show", "search-inputs hide");
			container.className = container.className.replace("masthead-search full", "masthead-search");
			button.className = button.className.replace("search-icon-button hide", "search-icon-button show");
			mainMenu.className = "main-navigation show";
		}
	};

	searchSubmit.onclick = function(event, target) {
		var searchTerm = document.getElementById("s").value;

		if (searchTerm === "") {
			search.className = search.className.replace("search-inputs show", "search-inputs hide");
			container.className = container.className.replace("masthead-search full", "masthead-search");
			button.className = button.className.replace("search-icon-button hide", "search-icon-button show");
			mainMenu.className = "main-navigation show";
			return false;
		}
	};
} )(jQuery);
