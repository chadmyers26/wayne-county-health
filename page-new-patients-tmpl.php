<?php
/**
 * Template Name: New Patients Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="new-patients" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('new-patients-hero-widget')){
						dynamic_sidebar('new-patients-hero-widget');
					}
				?>
			</div>
			<div class="information-wrapper">
				<div class="new-patients-info">
					<div class="heading">
						<?php if( get_field('new-patients-info-heading') ): ?>
							<h3><?php the_field('new-patients-info-heading'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="description">
						<?php if( get_field('new-patients-info-description') ): ?>
							<p><?php the_field('new-patients-info-description'); ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="new-patients-requirements">
					<div class="new-patients-requirements-wrapper">
						<hr><div class="title"><p>Requirements</p></div><hr>
					</div>
					<div class="new-patients-requirement-details">
						<ul>
							<li>
								<span><p>1</p></span>
								<div>
									<?php if( get_field('new-patients-requirement-one') ): ?>
										<p><?php the_field('new-patients-requirement-one'); ?></p>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<span><p>2</p></span>
								<div>
									<?php if( get_field('new-patients-requirement-two') ): ?>
										<p><?php the_field('new-patients-requirement-two'); ?></p>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<span><p>3</p></span>
								<div>
									<?php if( get_field('new-patients-requirement-three') ): ?>
										<p><?php the_field('new-patients-requirement-three'); ?></p>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<span><p>4</p></span>
								<div>
									<?php if( get_field('new-patients-requirement-four') ): ?>
										<p><?php the_field('new-patients-requirement-four'); ?></p>
									<?php endif; ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="new-patients-forms">
					<div class="new-patients-forms-wrapper">
						<hr><div class="title"><p>Forms</p></div><hr>
					</div>
					<div class="new-patients-forms-details">
						<div class="grid-container">
							<div class="grid-item">
								<h3>Welcome Letter</h3>
								<?php if( get_field('new-patients-welcome-letter') ): ?>
									<p><?php the_field('new-patients-welcome-letter'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Patient Registration</h3>
								<?php if( get_field('new-patients-registration') ): ?>
									<p><?php the_field('new-patients-registration'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Sliding Fee Application</h3>
								<?php if( get_field('new-patients-sliding-fee-application') ): ?>
									<p><?php the_field('new-patients-sliding-fee-application'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>HIPPA Privacy</h3>
								<?php if( get_field('new-patients-hippa-privacy') ): ?>
									<p><?php the_field('new-patients-hippa-privacy'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Patient Survey</h3>
								<?php if( get_field('new-patients-survey') ): ?>
									<p><?php the_field('new-patients-survey'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Emergency Contact</h3>
								<?php if( get_field('new-patients-emergency-contact') ): ?>
									<p><?php the_field('new-patients-emergency-contact'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Acknowledge of Receipt</h3>
								<?php if( get_field('new-patients-acknowledge-of-receipt') ): ?>
									<p><?php the_field('new-patients-acknowledge-of-receipt'); ?></p>
								<?php endif; ?>
							</div>
							<div class="grid-item">
								<h3>Contact Form</h3>
								<?php if( get_field('new-patients-contact-form') ): ?>
									<p><?php the_field('new-patients-contact-form'); ?></p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
