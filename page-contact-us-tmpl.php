<?php
/**
 * Template Name: Contact Us Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="contact-us" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('contact-us-hero-widget')){
						dynamic_sidebar('contact-us-hero-widget');
					}
				?>
			</div>
			<div class="contact-us-information-wrapper">
				<div class="contact-us-team-wrapper">
					<div class="title"><p>Contact Us</p></div>
				</div>
				<div class="contact-us-details">
					<div class="environmental-contact-info">
						<?php if( get_field('environmental-contact-info-heading') ): ?>
							<h3><?php the_field('environmental-contact-info-heading'); ?></h3>
						<?php endif; ?>
						<?php if( get_field('environmental-contact-info') ): ?>
							<?php the_field('environmental-contact-info'); ?>
						<?php endif; ?>
					</div>
					<div class="health-contact-info">
						<?php if( get_field('health-contact-info-heading') ): ?>
							<h3><?php the_field('health-contact-info-heading'); ?></h3>
						<?php endif; ?>
						<?php if( get_field('health-contact-info') ): ?>
							<?php the_field('health-contact-info'); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2575.4096601507904!2d-84.89993751959734!3d39.828175377750235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x884000555975fde3%3A0xdf671b99b0c4efff!2sWayne+County+Health+Department!5e0!3m2!1sen!2sus!4v1559912703463!5m2!1sen!2sus" width="auto" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
