<?php require_once "walker-aria-menu/aria-walker-nav-menu.php"; ?>

<?php
/**
 * Wayne County Health functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Wayne_County_Health
 */

if ( ! function_exists( 'wayne_county_health_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wayne_county_health_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Wayne County Health, use a find and replace
		 * to change 'wayne-county-health' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wayne-county-health', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wayne-county-health' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wayne_county_health_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wayne_county_health_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wayne_county_health_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'wayne_county_health_content_width', 640 );
}
add_action( 'after_setup_theme', 'wayne_county_health_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wayne_county_health_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wayne-county-health' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wayne-county-health' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wayne_county_health_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wayne_county_health_scripts() {
	wp_enqueue_style( 'wayne-county-health-style', get_stylesheet_uri() );
	wp_enqueue_style( 'wayne-county-health-stylesheet', get_theme_file_uri( '/assets/css/wayne-county-health.css' ));

	wp_enqueue_script( 'wayne-county-health-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'wayne-county-health-search', get_template_directory_uri() . '/js/search-toggle.js', array(), '20151215', true );
	wp_enqueue_script( 'textdomain-wai-aria', get_template_directory_uri() . '/walker-aria-menu/wai-aria.js', array( 'jquery' ), null );

	wp_enqueue_script( 'wayne-county-health-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wayne_county_health_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function html5_search_form( $form ) {
	$form = '<div id="search-inputs" class="search-inputs hide">
	<form class="search-form" role="search" method="get" id="search-form" action="http://waynecountyhealth.flywheelsites.com">
		<label class="search-field">
			<span class="screen-reader-text">What are you searching for?</span>
			<input type="text" id="s" name="s" class="search-input form-control" placeholder="Search"/>
		</label>
		<button type="submit" class="search-submit">
			<img src="/wp-content/themes/wayne-county-health/assets/icons/General/Search-Icon.svg" alt="Search Submit">
		</button>
	</form>
</div>';
	return $form;
}

add_filter( 'get_search_form', 'html5_search_form' );

register_sidebar( array(
	'name' => 'Footer Location Right',
	'id' => 'footer-location-right',
	'description' => 'Appears in the footer location right area',
	'before_widget' => '<div id="%1$s" class="location-right widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Footer Location Left',
	'id' => 'footer-location-left',
	'description' => 'Appears in the footer location left area',
	'before_widget' => '<div id="%1$s" class="location-left widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Home Hero Widget',
	'id' => 'home-hero-widget',
	'description' => 'Appears on the home page hero section',
	'before_widget' => '<div id="%1$s" class="home-hero widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Environmental Hero Widget',
	'id' => 'environmental-hero-widget',
	'description' => 'Appears on the environmental-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="environmental-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Home Information Text',
	'id' => 'home-information-text',
	'description' => 'Appears on the home page information section below the hero video',
	'before_widget' => '<div id="%1$s" class="home-info-text widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Home Information Nav',
	'id' => 'home-information-nav',
	'description' => 'Appears on the home page information section below the hero video',
	'before_widget' => '<div id="%1$s" class="home-info-nav widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Health Hero Widget',
	'id' => 'health-hero-widget',
	'description' => 'Appears on the health-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="health-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'New Patients Hero Widget',
	'id' => 'new-patients-hero-widget',
	'description' => 'Appears on the new-patients-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="new-patients-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Birth/Death Records Hero Widget',
	'id' => 'birth-death-records-hero-widget',
	'description' => 'Appears on the birth-death-records-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="birth-death-records-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'About Us Hero Widget',
	'id' => 'about-us-hero-widget',
	'description' => 'Appears on the about-us-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="about-us-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Contact Us Hero Widget',
	'id' => 'contact-us-hero-widget',
	'description' => 'Appears on the contact-us-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="contact-us-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'Disaster Preparedness Hero Widget',
	'id' => 'disaster-preparedness-hero-widget',
	'description' => 'Appears on the disaster-preparedness-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="disaster-preparedness-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );

register_sidebar( array(
	'name' => 'News Listings Hero Widget',
	'id' => 'news-listings-hero-widget',
	'description' => 'Appears on the news-listings-hero-widget page hero section',
	'before_widget' => '<div id="%1$s" class="news-listings-hero-widget widget %2$s">',
	'after_widget' => '</div>',
) );