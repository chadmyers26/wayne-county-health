<?php
/**
 * Template Name: Birth Death Records Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="birth-death-records" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('birth-death-records-hero-widget')){
						dynamic_sidebar('birth-death-records-hero-widget');
					}
				?>
			</div>
			<div class="information-wrapper">
				<div class="birth-death-records-info">
					<div class="heading">
						<?php if( get_field('birth-death-records-info-heading') ): ?>
							<h3><?php the_field('birth-death-records-info-heading'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="description">
						<?php if( get_field('birth-death-records-info-description') ): ?>
							<p><?php the_field('birth-death-records-info-description'); ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="birth-death-records-requirements">
					<div class="birth-death-records-requirements-wrapper">
						<hr><div class="title"><p>Requirements</p></div><hr>
					</div>
					<div class="birth-death-records-requirement-details">
						<ul>
							<li>
								<span><p>1</p></span>
								<div>
									<?php if( get_field('birth-death-records-requirement-one') ): ?>
										<p><?php the_field('birth-death-records-requirement-one'); ?></p>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<span><p>2</p></span>
								<div>
									<?php if( get_field('birth-death-records-requirement-two') ): ?>
										<p><?php the_field('birth-death-records-requirement-two'); ?></p>
									<?php endif; ?>
								</div>
							</li>
							<li>
								<span><p>3</p></span>
								<div>
									<?php if( get_field('birth-death-records-requirement-three') ): ?>
										<p><?php the_field('birth-death-records-requirement-three'); ?></p>
									<?php endif; ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="birth-death-records-application">
					<div class="birth-death-records-application-wrapper">
						<hr><div class="title"><p>Application</p></div><hr>
					</div>
					<div class="birth-death-records-application-details">
						<div class="birth-death-records-application-details-description">
							<?php if( get_field('birth-death-records-application-details-description') ): ?>
								<p><?php the_field('birth-death-records-application-details-description'); ?></p>
							<?php endif; ?>
						</div>
						<div class="birth-death-records-application-details-fees">
							<div class="grid-container">
								<div class="grid-item">
									<a class="button" href="http://waynecountyhealth.flywheelsites.com/wp-content/uploads/2019/07/birth-cert-doc.pdf">Birth</a>
									<?php if( get_field('birth-death-records-application-details-birth') ): ?>
										<p><?php the_field('birth-death-records-application-details-birth'); ?></p>
									<?php endif; ?>
								</div>
								<div class="grid-item">
									<a class="button" href="http://waynecountyhealth.flywheelsites.com/wp-content/uploads/2019/07/death-cert-doc.pdf">Death</a>
									<?php if( get_field('birth-death-records-application-details-death') ): ?>
										<p><?php the_field('birth-death-records-application-details-death'); ?></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="birth-death-records-application-details-ins-policy">
							<?php if( get_field('birth-death-records-application-details-ins-policy') ): ?>
								<p><?php the_field('birth-death-records-application-details-ins-policy'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
