<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="image-date-wrapper">
			<?php if( get_field('news-date') ): ?>
				<div class="date"><?php echo get_the_date('M d'); ?></div>
			<?php endif; ?>
			<div class="categories"><?php the_category(', '); ?></div>
		</div>
		<div class="content"><?php the_content(); ?></div>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
