<?php
/**
 * Template Name: Environmental Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="environmental" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('environmental-hero-widget')){
						dynamic_sidebar('environmental-hero-widget');
					}
				?>
				<div class="overlay-text">
					<p class="top-text">Protecting the</p>
					<p class="middle-text">health & safety of our</p>
					<p class="bottom-text">Community</p>
					<img class="arrow" src="/wp-content/themes/wayne-county-health/assets/icons/General/Arrow.svg" alt="More Content Below Icon">
				</div>
				<div class="hero-text-overlay"></div>
			</div>
			<div class="environmental-information-nav">
				<h2>How can we help you?</h2>
				<ul class="environmental-category-nav">
					<li>
						<a href="#cpr-fa">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/CPR_FA.svg" alt="Go to CPR & First Aid">
							<div class="title"><p>CPR &<br />First Aid</p></div>
						</a>
					</li>
					<li>
						<a href="#food-safety">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Food.svg" alt="Go to Food Safety">
							<div class="title"><p>Food<br />Safety</p></div>
						</a>
					</li>
					<li>
						<a href="#lead-healthy-homes">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/LeadSafety.svg" alt="Go to Lead & Healthy Homes">
							<div class="title"><p>Lead &<br />Healthy Homes</p></div>
						</a>
					</li>
					<li>
						<a href="#mosquito-control">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Mosquito.svg" alt="Go to Mosquito Control">
							<div class="title"><p>Mosquito<br />Control</p></div>
						</a>
					</li>
					<li>
						<a href="#disaster-preparedness">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Disaster.svg" alt="Go to Disaster Preparedness">
							<div class="title"><p>Disaster<br />Preparedness</p></div>
						</a>
					</li>
					<li>
						<a href="#rabies-education">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Bites.svg" alt="Go to Rabies Education">
							<div class="title"><p>Rabies<br />Education</p></div>
						</a>
					</li>
					<li>
						<a href="#sewage-disposal">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Sewage.svg" alt="Go to Sewage Disposal">
							<div class="title"><p>Sewage<br />Disposal</p></div>
						</a>
					</li>
					<li>
						<a href="#public-swimming">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/PoolSpa.svg" alt="Go to Public Swimming Pool & Spa Safety">
							<div class="title"><p>Public Swimming<br />Pool & Spa Safety</p></div>
						</a>
					</li>
				</ul>
			</div>
			<div class="environmental-content-wrapper">
				<div id="cpr-fa" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/CPR_FA.svg" alt="Go to CPR & First Aid">
					</div>
					<div class="right">
						<div class="cpr-heading">
							<?php if( get_field('cpr_&_first_aid_') ): ?>
								<h3><?php the_field('cpr_&_first_aid_'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="cpr-description">
							<?php if( get_field('cpr_&_first_aid_description') ): ?>
								<p><?php the_field('cpr_&_first_aid_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="class-info">
							<div class="class-title">
								<?php if( get_field('cpr_&_first_aid_class_title') ): ?>
									<p><?php the_field('cpr_&_first_aid_class_title'); ?></p>
								<?php endif; ?>
							</div>
							<div class="class-costs">
								<?php if( get_field('cpr_&_first_aid_class_costs') ): ?>
									<p><?php the_field('cpr_&_first_aid_class_costs'); ?></p>
								<?php endif; ?>
							</div>
						</div>
						<div class="class-details">
							<?php if( get_field('cpr_&_first_aid_class_descriptions') ): ?>
								<p><?php the_field('cpr_&_first_aid_class_descriptions'); ?></p>
							<?php endif; ?>
						</div>
						<div class="class-reference-link">
							<?php if( get_field('cpr_&_first_aid_class_link') ): ?>
								<p><?php the_field('cpr_&_first_aid_class_link'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="food-safety" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Food.svg" alt="Go to Food Safety">
					</div>
					<div class="right">
						<div class="fs-heading">
							<?php if( get_field('fs-heading') ): ?>
								<h3><?php the_field('fs-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="fs-description">
							<?php if( get_field('fs_description') ): ?>
								<p><?php the_field('fs_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="fs-links">
							<?php if( get_field('fs_links') ): ?>
								<p><?php the_field('fs_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="lead-healthy-homes" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/LeadSafety.svg" alt="Go to Lead & Healthy Homes">
					</div>
					<div class="right">
						<div class="lhh-heading">
							<?php if( get_field('lhh-heading') ): ?>
								<h3><?php the_field('lhh-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="lhh-description">
							<?php if( get_field('lhh_description') ): ?>
								<p><?php the_field('lhh_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="lhh-links">
							<?php if( get_field('lhh_links') ): ?>
								<p><?php the_field('lhh_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="mosquito-control" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Mosquito.svg" alt="Go to Mosquito Control">
					</div>
					<div class="right">
						<div class="mc-heading">
							<?php if( get_field('mc-heading') ): ?>
								<h3><?php the_field('mc-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="mc-description">
							<?php if( get_field('mc_description') ): ?>
								<p><?php the_field('mc_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="mc-links">
							<?php if( get_field('mc_links') ): ?>
								<p><?php the_field('mc_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="disaster-preparedness" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Disaster.svg" alt="Go to Disaster Preparedness">
					</div>
					<div class="right">
						<div class="dp-heading">
							<?php if( get_field('dp-heading') ): ?>
								<h3><?php the_field('dp-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="dp-description">
							<?php if( get_field('dp_description') ): ?>
								<p><?php the_field('dp_description'); ?></p>
							<?php endif; ?>
						</div>
						<div class="dp-links">
							<?php if( get_field('dp_links') ): ?>
								<p><?php the_field('dp_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="rabies-education" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Bites.svg" alt="Go to Rabies Education">
					</div>
					<div class="right">
						<div class="rabies-heading">
							<?php if( get_field('rabies-heading') ): ?>
								<h3><?php the_field('rabies-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="rabies-description">
							<?php if( get_field('rabies_description') ): ?>
								<p><?php the_field('rabies_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="rabies-links">
							<?php if( get_field('rabies_links') ): ?>
								<p><?php the_field('rabies_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="sewage-disposal" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/Sewage.svg" alt="Go to Sewage Disposal">
					</div>
					<div class="right">
						<div class="sewage-heading">
							<?php if( get_field('sewage-heading') ): ?>
								<h3><?php the_field('sewage-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="sewage-description">
							<?php if( get_field('sewage_description') ): ?>
								<p><?php the_field('sewage_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="sewage-links">
							<?php if( get_field('sewage_links') ): ?>
								<p><?php the_field('sewage_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="public-swimming" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Environmental/PoolSpa.svg" alt="Go to Public Swimming Pool & Spa Safety">
					</div>
					<div class="right">
						<div class="ps-heading">
							<?php if( get_field('ps-heading') ): ?>
								<h3><?php the_field('ps-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="ps-description">
							<?php if( get_field('ps_description') ): ?>
								<p><?php the_field('ps_description'); ?></p>
							<?php endif; ?>
						</div>
						<hr>
						<div class="ps-links">
							<?php if( get_field('ps_links') ): ?>
								<p><?php the_field('ps_links'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
