<?php
/**
 * Template Name: News Item Page Tpl
 *
 * This is the template that displays a individual news blog posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

get_header();
?>

	<div id="news-wrapper" class="internal-content-area has-aside">
		<main id="main" class="site-main">
		<?php
				$query = new WP_Query(array(
					'post_type'=>'post',
					'post_status'=>'publish',
					'posts_per_page'=>-1
				));
				if( $query->have_posts() ) : ?>
					<ul class="news-items">
						<?php
						while( $query->have_posts() ) : $query->the_post(); ?>
							<li>
								<h2><?php the_title(); ?></h2>
								<div class="posted-info">
									<?php if( get_field('news-date') ): ?>
										<div class="date"><?php the_date('M, d'); ?></div>
									<?php endif; ?>
									<div class="categories">
										<?php the_category(', '); ?>
									</div>
								</div>
								<div class="image">
									<?php the_post_thumbnail(); ?>
								</div>
								<?php the_content(); ?>
							</li>
					</ul>
				<?php
					endwhile;
				else:

				endif;
				wp_reset_postdata();
			?>
		</main><!-- #main -->
		<aside>
			<?php
				if(is_active_sidebar('news-sidebar')){
				dynamic_sidebar('news-sidebar');
				}
			?>
		</aside>
	</div><!-- #primary -->

<?php
get_footer();
