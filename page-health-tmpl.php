<?php
/**
 * Template Name: Health Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="health" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('health-hero-widget')){
						dynamic_sidebar('health-hero-widget');
					}
				?>
				<div class="overlay-text">
					<p class="top-text">Experienced Faculty.</p>
					<p class="bottom-text">Friendly Physicians.</p>
					<img class="arrow" src="/wp-content/themes/wayne-county-health/assets/icons/General/Arrow.svg" alt="More Content Below Icon">
				</div>
				<div class="hero-text-overlay"></div>
			</div>
			<div class="health-information-nav">
				<h2>How can we help you?</h2>
				<ul class="health-category-nav">
					<li>
						<a href="#adult-services">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/AdultServices.svg" alt="Go to Adult Services">
							<div class="title"><p>Adult<br />Services</p></div>
						</a>
					</li>
					<li>
						<a href="#behavioral-health">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/BehavioralHealth.svg" alt="Go to Behavioral Health">
							<div class="title"><p>Behavioral<br />Health</p></div>
						</a>
					</li>
					<li>
						<a href="#dental-services">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/DentalServices.svg" alt="Go to Dental Services">
							<div class="title"><p>Dental<br />Program</p></div>
						</a>
					</li>
					<li>
						<a href="#family-planning">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/FamilyPlanning.svg" alt="Go to Family Planning">
							<div class="title"><p>Family<br />Planning</p></div>
						</a>
					</li>
					<li>
						<a href="#immunization-program">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Immunization.svg" alt="Go to Immunization Program">
							<div class="title"><p>Immunization<br />Program</p></div>
						</a>
					</li>
					<li>
						<a href="#maternity-services">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/MaternityService.svg" alt="Go to Maternity Services">
							<div class="title"><p>Maternity<br />Services</p></div>
						</a>
					</li>
					<li>
						<a href="#obgyn">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/ObstGyn.svg" alt="Go to Obstetrics & Gynecology">
							<div class="title"><p>Obstetrics &<br />Gynecology</p></div>
						</a>
					</li>
					<li>
						<a href="#on-site-lab">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Lab.svg" alt="Go to On-Site Lab">
							<div class="title"><p>On-Site<br />Lab</p></div>
						</a>
					</li>
					<li>
						<a href="#pediatric-services">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Pediatric.svg" alt="Go to Pediatric Services">
							<div class="title"><p>Pediatric<br />Services</p></div>
						</a>
					</li>
					<li>
						<a href="#pregnancy-testing">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/PregnancyTest.svg" alt="Go to Pregnancy Testing">
							<div class="title"><p>Pregnancy<br />Testing</p></div>
						</a>
					</li>
					<li>
						<a href="#std-program">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/STD.svg" alt="Go to STD Program">
							<div class="title"><p>STD<br />Program</p></div>
						</a>
					</li>
					<li>
						<a href="#tuberculosis-clinic">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/TB.svg" alt="Go to Tuberculosis Clinic">
							<div class="title"><p>Tuberculosis<br />Clinic</p></div>
						</a>
					</li>
				</ul>
			</div>
			<div class="new-patients-info">
				<div class="heading">
					<?php if( get_field('new-patients-heading') ): ?>
						<h3><?php the_field('new-patients-heading'); ?></h3>
					<?php endif; ?>
				</div>
				<div class="description">
					<?php if( get_field('new-patients-description') ): ?>
						<p><?php the_field('new-patients-description'); ?></p>
					<?php endif; ?>
				</div>
				<a href="http://waynecountyhealth.flywheelsites.com/new-patients/">Join</a>
			</div>
			<div class="health-content-wrapper">
				<div id="adult-services" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/AdultServices.svg" alt="Go to Adult Services">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('adult-services-heading') ): ?>
								<h3><?php the_field('adult-services-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('adult-services-description') ): ?>
								<p><?php the_field('adult-services-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="behavioral-health" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/BehavioralHealth.svg" alt="Go to Behavioral Health">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('behavioral-health-heading') ): ?>
								<h3><?php the_field('behavioral-health-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('behavioral-health-description') ): ?>
								<p><?php the_field('behavioral-health-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="dental-services" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/DentalServices.svg" alt="Go to Dental Services">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('dental-services-heading') ): ?>
								<h3><?php the_field('dental-services-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('dental-services-description') ): ?>
								<p><?php the_field('dental-services-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="family-planning" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/FamilyPlanning.svg" alt="Go to Family Planning">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('family-planning-heading') ): ?>
								<h3><?php the_field('family-planning-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('family-planning-description') ): ?>
								<p><?php the_field('family-planning-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="immunization-program" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Immunization.svg" alt="Go to Immunization Program">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('immunization-program-heading') ): ?>
								<h3><?php the_field('immunization-program-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('immunization-program-description') ): ?>
								<p><?php the_field('immunization-program-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="maternity-services" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/MaternityService.svg" alt="Go to Maternity Services">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('maternity-services-heading') ): ?>
								<h3><?php the_field('maternity-services-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('maternity-services-description') ): ?>
								<p><?php the_field('maternity-services-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="obgyn" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/ObstGyn.svg" alt="Go to Obstetrics & Gynecology">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('obgyn-heading') ): ?>
								<h3><?php the_field('obgyn-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('obgyn-description') ): ?>
								<p><?php the_field('obgyn-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="on-site-lab" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Lab.svg" alt="Go to On-Site Lab">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('on-site-lab-heading') ): ?>
								<h3><?php the_field('on-site-lab-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('on-site-lab-description') ): ?>
								<p><?php the_field('on-site-lab-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="pediatric-services" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/Pediatric.svg" alt="Go to Pediatric Services">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('pediatric-services-heading') ): ?>
								<h3><?php the_field('pediatric-services-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('pediatric-services-description') ): ?>
								<p><?php the_field('pediatric-services-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="pregnancy-testing" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/PregnancyTest.svg" alt="Go to Pregnancy Testing">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('pregnancy-testing-heading') ): ?>
								<h3><?php the_field('pregnancy-testing-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('pregnancy-testing-description') ): ?>
								<p><?php the_field('pregnancy-testing-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="std-program" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/STD.svg" alt="Go to STD Program">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('std-program-heading') ): ?>
								<h3><?php the_field('std-program-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('std-program-description') ): ?>
								<p><?php the_field('std-program-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div id="tuberculosis-clinic" class="content-box">
					<div class="left">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/Health/TB.svg" alt="Go to Tuberculosis Clinic">
					</div>
					<div class="right">
						<div class="heading">
							<?php if( get_field('tuberculosis-clinic-heading') ): ?>
								<h3><?php the_field('tuberculosis-clinic-heading'); ?></h3>
							<?php endif; ?>
						</div>
						<div class="description">
							<?php if( get_field('tuberculosis-clinic-description') ): ?>
								<p><?php the_field('tuberculosis-clinic-description'); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
