<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<section id="search-results" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="heading">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Results for: %s', 'wayne-county-health' ), '<span>'.'"' . get_search_query() . '"'.'</span>' );
					?>
				</h1>

				<form class="search-page-form" role="search" method="get" id="search-page-form" action="http://waynecountyhealth.flywheelsites.com">
					<div>
						<label for="s" class="screen-reader-text">What are you looking for?</label>
						<input type="text" id="s" name="s" class="search-input form-control" placeholder="<?php echo get_search_query() ?>"/>
					</div>
					<button type="submit" class="search-submit">
						<img src="/wp-content/themes/wayne-county-health/assets/icons/General/Search-Icon.svg" alt="Search Submit">
					</button>
				</form>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;
		else : ?>

			<header class="page-header">
				<h1 class="heading">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Sorry, no results found for: %s', 'wayne-county-health' ), '<span>'.'"' . get_search_query() . '"'.'</span>' );
					?>
				</h1>

				<form class="search-page-form" role="search" method="get" id="search-page-form" action="http://waynecountyhealth.flywheelsites.com">
					<div>
						<label for="s" class="screen-reader-text">What are you looking for?</label>
						<input type="text" id="s" name="s" class="search-input form-control" placeholder="<?php echo get_search_query() ?>"/>
						<button type="submit" class="search-submit">
							<img src="/wp-content/themes/wayne-county-health/assets/icons/General/Search-Icon.svg" alt="Search Submit">
						</button>
					</div>
				</form>

				<p>Please try again with some different keywords.</p>
			</header><!-- .page-header -->

		<?php endif;
		?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
