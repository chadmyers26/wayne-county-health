<?php
/**
 * Template Name: About Us Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="about-us" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('about-us-hero-widget')){
						dynamic_sidebar('about-us-hero-widget');
					}
				?>
			</div>
			<div class="information-wrapper">
				<div class="about-us-info">
					<div class="heading">
						<?php if( get_field('about-us-info-heading') ): ?>
							<h3><?php the_field('about-us-info-heading'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="description">
						<?php if( get_field('about-us-info-description') ): ?>
							<p><?php the_field('about-us-info-description'); ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="about-us-team">
					<div class="about-us-team-wrapper">
						<hr><div class="title"><p>Our Team</p></div><hr>
					</div>
					<div class="about-us-details">
						<ul>
							<li>
								<img src="<?php if( get_field('team-member-image-1') ): ?>
									<?php the_field('team-member-image-1'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-1') ): ?>
									<div class="member-name"><?php the_field('team-member-name-1'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-1') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-1'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-2') ): ?>
									<?php the_field('team-member-image-2'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-2') ): ?>
									<div class="member-name"><?php the_field('team-member-name-2'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-2') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-2'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-3') ): ?>
									<?php the_field('team-member-image-3'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-3') ): ?>
									<div class="member-name"><?php the_field('team-member-name-3'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-3') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-3'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-4') ): ?>
									<?php the_field('team-member-image-4'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-4') ): ?>
									<div class="member-name"><?php the_field('team-member-name-4'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-4') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-4'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-5') ): ?>
									<?php the_field('team-member-image-5'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-5') ): ?>
									<div class="member-name"><?php the_field('team-member-name-5'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-5') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-5'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-6') ): ?>
									<?php the_field('team-member-image-6'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-6') ): ?>
									<div class="member-name"><?php the_field('team-member-name-6'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-6') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-6'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-7') ): ?>
									<?php the_field('team-member-image-7'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-7') ): ?>
									<div class="member-name"><?php the_field('team-member-name-7'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-7') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-7'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-8') ): ?>
									<?php the_field('team-member-image-8'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-8') ): ?>
									<div class="member-name"><?php the_field('team-member-name-8'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-8') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-8'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-9') ): ?>
									<?php the_field('team-member-image-9'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-9') ): ?>
									<div class="member-name"><?php the_field('team-member-name-9'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-9') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-9'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-10') ): ?>
									<?php the_field('team-member-image-10'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-10') ): ?>
									<div class="member-name"><?php the_field('team-member-name-10'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-10') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-10'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-11') ): ?>
									<?php the_field('team-member-image-11'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-11') ): ?>
									<div class="member-name"><?php the_field('team-member-name-11'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-11') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-11'); ?></div>
								<?php endif; ?>
							</li>
							<li>
								<img src="<?php if( get_field('team-member-image-12') ): ?>
									<?php the_field('team-member-image-12'); ?>
								<?php endif; ?>"/>
								<?php if( get_field('team-member-name-12') ): ?>
									<div class="member-name"><?php the_field('team-member-name-12'); ?></div>
								<?php endif; ?>
								<?php if( get_field('team-member-practices-12') ): ?>
									<div class="member-practices"><?php the_field('team-member-practices-12'); ?></div>
								<?php endif; ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
