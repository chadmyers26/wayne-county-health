<?php
/**
 * Template Name: News Listings Page Tpl
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wayne_County_Health
 */

get_header();
?>

	<div id="news-listings" class="content-area">
		<main id="home" class="site-main">
			<div class="hero-image">
				<?php
					if(is_active_sidebar('news-listings-hero-widget')){
						dynamic_sidebar('news-listings-hero-widget');
					}
				?>
			</div>
			<div class="news-listings-information-wrapper">
				<div class="news-listings-wrapper">
					<div class="title"><p>News</p></div>
				</div>
				<div class="most-recent-news-items-wrapper">
					<?php $the_query = new WP_Query(array(
							'post_type'=>'post',
							'post_status'=>'publish',
							'posts_per_page' => 1,
						));
					?>
					<?php if ( $the_query->have_posts() ) : ?>
					<ul class="news-item">
						<?php
						while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
							<li>
								<div class="image">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="new-item-content-wrapper">
									<div class="categories <?php $categories = get_the_category(); echo esc_html( $categories[0]->name ); ?>">
										<?php the_category(', '); ?>
									</div>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php wp_reset_postdata(); ?>
					<?php else: ?>
					<?php endif; ?>
				</div>

				<div class="news-listings-slider">
					<?php $the_query = new WP_Query(array(
							'post_type'=>'post',
							'post_status'=>'publish',
							'posts_per_page' => 9,
							'offset' => 1,
						));
					?>
					<?php if ( $the_query->have_posts() ) : ?>
					<div class="news-items">
						<?php
						while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
							<div class="item-container">
								<div class="image">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="new-item-content-wrapper">
									<div class="categories <?php $categories = get_the_category(); echo esc_html( $categories[0]->name ); ?>">
										<?php the_category(', '); ?>
									</div>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<?php wp_reset_postdata(); ?>
					<?php else: ?>
					<?php endif; ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
